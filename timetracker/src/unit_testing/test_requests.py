#!/usr/local/bin/python

import requests
from datetime import timedelta, datetime


def send_attendance():
    data = {
        'user_id': 1
    }
    print("post params: ", data)
    out = requests.post("http://localhost/user", json=data)
    print(out.json())


def get_user_data():
    data = {
        'user_id': 1,
    }
    out = requests.get("http://localhost/user", params=data)
    print(out)
    if out:
        print(out.json())


def set_subscription_status():
    data = {
        'user_id': 1,
        'subscribe': True,
    }
    out = requests.post("http://localhost/user/subscribe", json=data)
    print(out)
    if out:
        print(out.json())


def get_subscription_status():
    data = {
        'user_id': 1,
    }
    out = requests.get("http://localhost/user/subscribe", params=data)
    print(out)
    if out:
        print(out.json())

def test_last_interaction():
    # not tested
    params = {
        'time_delta': 5
    }
    out = requests.get('http://bot/user/last_interaction', params=params)
    if out:
        print('Response', out.json())
    else:
        print("RESPONSE: ", out)


if __name__ == "__main__":
    # send_attendance()
    # get_user_data()
    # set_subscription_status()
    # get_subscription_status()
    # test_last_interaction()
    quit()
