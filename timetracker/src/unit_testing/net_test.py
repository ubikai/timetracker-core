from subprocess import check_output
import json


def get_container_ip(cont_name):
    out = check_output(["dig", "+short", cont_name])
    out = out.decode("utf-8")
    out = out.split('\n', 1)[0]
    return out if out is not "" else None


if __name__ == '__main__':
    assert get_container_ip('roy_dev_bot_1') == '172.20.0.7'
    assert get_container_ip('t_1') is None
