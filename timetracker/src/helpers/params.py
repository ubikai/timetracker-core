from app.models import User


def get_param(**params):
    """
    Creates a dict with params to be sent to chat bot service
        If user is
    :param params: user= u, action= 'pre_workout', training_id= 3 / action= 'pre_workout', training_id= 3
    :return: {'user_id': 2, 'action': 'pre_workout', 'training_id': 3} / {'action': 'pre_workout', 'training_id': 3}
    """
    user = params.get('user')

    if user and type(user) is User:

        del(params['user'])

        if user.subscribed:
            params['user_id'] = user.bot_id
            return params
        else:
            return None

    return params
