import requests
from app import app


def send_check_work_request(chatbot_id):
    data = {
        "user_id": chatbot_id,
        "action": "time_tracking.check_work_request"
    }
    out = requests.post("http://bot/facebook/work_mode", json=data)


def get_user_name(bot_id):
    out = requests.get('http://'+app.config['BOT_IP']+'/user?bot_id='+str(bot_id))
    if out:
        data = out.json()
        return data
    return None
