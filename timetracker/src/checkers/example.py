#!/usr/local/bin/python

import sys

sys.path.append("/timetracker/src")

# import modules
from app.models import *
import requests
from os import environ
from helpers.networking import get_container_ip
from multiprocessing import Process


def send_req(cont_ip, params):
    out = requests.post("http://{}/facebook/special_flow".format(cont_ip), json=params)

    print(out)
    if out:
        print(out.json())


def get_params():
    # Todo: add filter for subscribed users
    users = User.query.filter(User.category == Category.get_category("default")).all()
    params = []
    for user in users:
        print(user)
        param = {
            'user_id': user.bot_id,
            'action': 'FLOW_NAME'
        }
        params.append(param)
    return params


if __name__ == '__main__':
    cont_ip = get_container_ip('{}_bot_1'.format('chatbot-core'))
    if not cont_ip:
        print("cannot find container IP")
        quit()

    params = get_params()

    for param in params:
        t = Process(target=send_req, args=(cont_ip, param))
        t.start()
