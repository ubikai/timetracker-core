from helpers.networking import get_container_ip
from os import environ


class Config(object):
    APPLICATION_ROOT = 'app/'
    PROJECT_NAME = environ.get("PROJECT_NAME", default=None)
    root_psw = environ.get("MYSQL_ROOT_PASSWORD", default=None)
    hostname = environ.get("MYSQL_HOSTNAME", default=None)
    db_name = environ.get("MYSQL_DATABASE", default=None)

    SQLALCHEMY_DATABASE_URI = 'mysql://root:{}@{}/{}'.format(root_psw, hostname, db_name)
    # SQLALCHEMY_DATABASE_URI = 'mysql://root:TTPass2018@tt_db/time_tracker_db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    BOT_IP = get_container_ip('{}_bot_1'.format(PROJECT_NAME))


class DevelopmentConfig(Config):
    DEBUG = True  # Turns on debugging features in Flask
    TESTING = True


class ProductionConfig(Config):
    DEBUG = False  # Turns on debugging features in Flask
    TESTING = False
