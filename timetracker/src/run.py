import sys

sys.path.append("/timetracker/src")

from app import app, db
from app import models


@app.shell_context_processor
def make_shell_context():
    return {
        'db': db,
        'models': models,
    }


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
