from app import db
from datetime import datetime, timedelta, date


class BaseMixin(object):

    def submit(self):
        try:
            db.session.add(self)
            db.session.commit()
        except Exception as ex:
            db.session.rollback()
            print("DB ERROR: ", ex._message())

    @classmethod
    def create(cls, **kwargs):
        obj = cls(**kwargs)
        try:
            db.session.add(obj)
            db.session.commit()
        except Exception as ex:
            db.session.rollback()
            print("DB ERROR: ", ex._message())

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
        except Exception as ex:
            db.session.rollback()
            print("DB ERROR: ", ex._message())


class User(BaseMixin, db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    bot_id = db.Column(db.Integer, unique=True)

    subscribed = db.Column(db.Boolean, default=False)

    category_id = db.Column(db.Integer, db.ForeignKey("categories.id"))
    category = db.relationship("Category", uselist=False)

    @staticmethod
    def get_user(bot_id):
        user = User.query.filter(User.bot_id == bot_id).first()
        return user

    @staticmethod
    def create_user(bot_id):
        user = User.query.filter(User.bot_id == bot_id).first()
        if not user:
            user = User(bot_id=bot_id)
            user.category = Category.get_category('user')
        return user

    def __repr__(self):
        return '<User id={}, bot_id={}>'.format(self.id, self.bot_id)


class Category(BaseMixin, db.Model):
    __tablename__ = 'categories'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(1024), unique=True)

    @staticmethod
    def get_category(category_name):
        category = Category.query.filter(Category.name == category_name).first()
        if not category:
            category = Category(name=category_name)
        return category


#
# Broadcast Module
#


class BroadcastType(BaseMixin, db.Model):
    __tablename__ = 'broadcast_type'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))

    def __repr__(self):
        return '<broadcast_type id = {}, name={}>' \
            .format(self.id, self.name)


class Broadcast(BaseMixin, db.Model):
    __tablename__ = 'broadcast'

    id = db.Column(db.Integer, primary_key=True)
    type_id = db.Column(db.Integer, db.ForeignKey('broadcast_type.id'))
    message = db.Column(db.String(1024))
    time = db.Column(db.DateTime)
    buttons = db.Column(db.JSON, default=[])
    processed = db.Column(db.Integer, default=0)

    type = db.relationship("BroadcastType")

    def __repr__(self):
        return '<Broadcast id = {}, broadcast_type id={}, message={}>' \
            .format(self.id, self.type_id, self.message)


class BroadcastData(BaseMixin, db.Model):
    __tablename__ = 'broadcast_data'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    broadcast_id = db.Column(db.Integer, db.ForeignKey('broadcast.id'))
    value = db.Column(db.String(512))

    user = db.relationship("User")
    broadcast = db.relationship("Broadcast")

    def __repr__(self):
        return '<BroadcastData id = {}, user_id={}, broadcast_id={}, value={}>' \
            .format(self.id, self.user_id, self.broadcast_id, self.value)
