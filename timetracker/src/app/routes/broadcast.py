from app.routes import *


@app.route("/broadcast/schedule", methods=['POST', 'GET'])
def broadcast_schedule():
    if request.method == 'GET':
        # user_id = request.args.get('user_id')

        return {"STATUS": 'GET METHOD: OK'}

    elif request.method == 'POST':
        data = request.get_json()

        if data:
            if len(data['message']) > 1024:
                return error_response("Message len is too long (char 1024)")
            br_type = BroadcastType.query.filter(BroadcastType.name == data['message_type']).first()
            if not br_type:
                br_type = BroadcastType(name=data['message_type'])
                br_type.submit()
            br_ = Broadcast(type_id=br_type.id, message=data['message'], time=datetime.fromtimestamp(data['time']))
            if data.get('buttons'):
                br_.buttons = data['buttons']
            br_.submit()
        else:
            return error_response("No data recived")

        return success_response("Broadcast scheduled successfully")


@app.route("/broadcast", methods=['POST', 'GET'])
def broadcast():
    if request.method == 'GET':
        return {"STATUS": 'GET METHOD: OK'}

    elif request.method == 'POST':
        try:
            data = request.get_json()
            data_keys = ['user_id', 'broadcast_id', 'value']
            data_val = {}

            print(data, file=sys.stderr)

            for key in data_keys:
                if data.get(key):
                    data_val[key] = data[key]
                else:
                    return error_response("Keyword {} not recived".format(key))

            print(data_val, file=sys.stderr)
            user = User.get_user(data_val['user_id'])

            br = Broadcast.query.get(data_val['broadcast_id'])

            brd = BroadcastData(user=user, broadcast=br, value=br)
            brd.submit()
        except (NameError, SyntaxError, KeyError, AttributeError, Exception) as ex:
            print('ERROR: ', ex, file=sys.stderr)

        return success_response("Broadcast response saved successfully")

