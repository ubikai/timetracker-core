from app.routes import *
import dateutil.parser


@app.route("/user", methods=['POST', 'GET'])
def user():
    if request.method == 'POST':
        # get the json
        data = request.get_json()
        if not data:
            return error_response("no data recieved")

        # get user
        bot_id = data.get('bot_id')
        if not bot_id:
            return error_response("missing bot_id")

        if not User.get_user(bot_id):
            user = User.create_user(bot_id)
        user.submit()
        return {"STATUS": "User created"}

    if request.method == 'GET':
        """
        @:param data {'bot_id': <INT> or 'all': 1}
        @:return: [user(s)]
        """
        data = request.args

        if not data:
            return error_response("no data recieved")

        user_id = request.args.get('user_id')
        all = request.args.get('all')

        if user_id:
            # not nEEded?
            error_response("Can't return single user data because there is no CODE for dees")
            pass
        elif all:
            out = []
            users = User.query.all()

            for user in users:
                out.append({
                    'bot_id': user.bot_id,
                    'category': {'name': user.category.name, 'id': user.category.id},
                    'subscribed': user.subscribed
                })
            return {"data": out}
        else:
            error_response("No user_id(s)")


@app.route("/user/subscribe", methods=['POST', 'GET'])
def subscribe_status():
    if request.method == 'POST':
        # get the json
        data = request.get_json()

        # check if json exists
        if not data:
            return error_response("no data recieved")

        # get user
        user_id = data.get('user_id')
        if not user_id:
            return error_response("missing user_id")

        # get subscribe
        subscribe = data.get('subscribe')
        if subscribe is None:
            return error_response("missing subscribe")

        user = User.get_user(user_id)

        user.subscribed = subscribe

        user.submit()
        # return
        return {"STATUS": "subscription changed"}

    if request.method == 'GET':
        # get user
        user_id = request.args.get('user_id')
        if not user_id:
            return error_response("missing user_id")

        user = User.get_user(user_id)

        return {"user_subscribed": user.subscribed}


@app.route("/user/rank", methods=['POST', 'GET'])
def user_rank():
    if request.method == 'GET':
        """
        @:param data: {'bot_id': <INT>}
        @:rtype: list
        @:return: [
            {
                'category': {'name': <STR>, 'id': <INT>},
            }, 
        ]
        """
        data = request.args
        if not data:
            return error_response("no data recieved")

        bot_id = data.get('bot_id')
        if not bot_id:
            return error_response("no bot_id recieved")

        user = User.get_user(bot_id)
        out = {
            'category': {'name': user.category.name, 'id': user.category.id},
        }

        return out

    elif request.method == 'POST':
        # fail check in platform
        data = request.get_json()
        bot_id = data['bot_id']
        category = data['category']

        print(bot_id, category, file=sys.stderr)

        user = User.get_user(bot_id)
        user.category = Category.get_category(category)
        user.submit()
        return {"STATUS": 'OK'}

    return {"STATUS": 'METHOD UNKNOWN'}
