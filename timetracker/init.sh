if [ ! -d "/usr/src/migrations" ]; then
  # Control will enter here if $DIRECTORY doesn't exist.
  flask db init -d /usr/src/migrations
fi
flask db migrate -m "up" -d /usr/src/migrations
flask db upgrade -d /usr/src/migrations

service cron start

chmod 700 /timetracker/src/checkers/*

if test $FLASK_ENV = 'development'; then
    flask run --host=0.0.0.0 --port=80
else
    /usr/sbin/apache2ctl -D FOREGROUND
fi
